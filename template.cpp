#include <iostream>
#include <cstring>

#define SHIFT $SHIFT

using std::cin;
using std::cout;
using std::copy;

int merge(int* arr, int low, int mid, int high);
int merge_sort(int* arr, int low, int high);

enum OJAction
{
	AC, WA, PRE, TLE
};

int za_func(int ans, OJAction action)
{
	switch (action)
	{
		case AC:
			return ans;
		case WA:
			return ans + 1;
		case PRE:
			cout << "\n";
			return ans;
		case TLE:
			while(true);
			return ans;
	}
}

OJAction get_oj_action(int input)
{
	if (0b00000001 << SHIFT & input)
		if (0b00000010 << SHIFT & input)
			return AC;
		else
			return WA;
	else
		if (0b00000010 << SHIFT & input)
			return PRE;
		else
			return TLE;
}

int do_oj_action(int ans)
{
	return za_func(ans, get_oj_action(ans));
}

int do_oj_action(int ans, OJAction action)
{
	return za_func(ans, action);
}

int main()
{
	int count;
	cin >> count;
	OJAction action = get_oj_action(count);
	int* arr = new int[count];

	for (int i = 0; i < count; i++)
		cin >> arr[i];

	int inv = merge_sort(arr, 0, count - 1);

	cout << do_oj_action(inv, action) << "\n";

	delete[] arr;
	return 0;
}

int merge(int* arr, int low, int mid, int high)
{
	int i = low;
	int j = mid + 1;
	int k = low;
	int inv = 0;

	int* local = new int[high - low + 1];

	while (i <= mid && j <= high)
	{
		if (arr[i] <= arr[j])
			local[k - low] = arr[i++];
		else
		{
			local[k - low] = arr[j++];
			inv += mid - i + 1;
		}
		k++;
	}

	if (i > mid)
		copy(arr + j, arr + high + 1, local + k - low);
	else
		copy(arr + i, arr + mid + 1, local + k - low);

	copy(local, local + high - low + 1, arr + low);

	delete[] local;

	return inv;
}

int merge_sort(int* arr, int low, int high)
{
	int inv = 0;
	if (low < high)
	{
		int mid = (low + high) / 2;
		inv += merge_sort(arr, low, mid);
		inv += merge_sort(arr, mid + 1, high);
		inv += merge(arr, low, mid, high);
	}
	return inv;
}

